package Proyecto;

public class Main {

    public static void main(String[] args) {

        //tamaño de la matriz
        int tamanio = 5;

        char[][] matriz = new char[tamanio][tamanio];
        //Espacios negros de la matriz
        String[] vectorEspaciosNegro = {"0 0", "0 1", "0 3", "0 4", "1 0", "1 1", "1 3", "1 4", "2 0", "3 0", "3 1", "3 3", "3 4"};
        //Posición de cada palabra
        String[] vectorPosiciones = {"0 2 v", "2 1 h", "4 0 h"};
        //Pistas de las palabras
        String[] vectorPistas = {"Conjunto de lugares donde habitan personas", "Forma de cocinar un alimento", "Límite de un lugar o una acción"};
        //Palabras por adivinar
        String[] vectorPalabras = {"casas", "asar", "hasta"};

        //For para agregar los espacios en negro a la matriz
        for (int i = 0; i < vectorEspaciosNegro.length; i++) {
            String[] posiciones = vectorEspaciosNegro[i].split(" ");
            int fila = Integer.parseInt(posiciones[0]);
            int columna = Integer.parseInt(posiciones[1]);
            matriz[fila][columna] = '%';
        }

        //ejemplo para agregar una palabra a la matriz
        agregarPalabra(vectorPalabras[0], vectorPosiciones[0], matriz);

        //Doble For para imprimir una matriz
        for (int i = 0; i < tamanio; i++) {
            for (int j = 0; j < tamanio; j++) {
                System.out.print(matriz[i][j] + "\t");
            }
            System.out.println();
        }

    }

    public static void agregarPalabra(String palabra, String posicion, char[][] matriz) {
        String[] vectorTemporal = posicion.split(" ");
        int filaInicial = Integer.parseInt(vectorTemporal[0]);
        int columna = Integer.parseInt(vectorTemporal[1]);

        char[] vectorPalabra = palabra.toCharArray();

        if (vectorTemporal[2].equals("v")) {
            for(int fila = filaInicial; fila < palabra.length(); fila++) {
               matriz[fila][columna] = vectorPalabra[fila];
            }
        } else {
            //Falta de implementar
        }
    }
}

